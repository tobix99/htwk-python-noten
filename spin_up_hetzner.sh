#!/bin/bash

API_TOKEN=$(<api.token) 

echo "Checking Dependencies"
#check for curl, ssh
hash curl 2>/dev/null || { echo >&2 "I require curl but it's not installed.  Aborting."; exit 1; }
hash ssh 2>/dev/null || { echo >&2 "I require ssh but it's not installed.  Aborting."; exit 1; }
hash ssh-keygen 2>/dev/null || { echo >&2 "I require ssh-keygen but it's not installed.  Aborting."; exit 1; }
hash jq 2>/dev/null || { echo >&2 "I require jq but it's not installed.  Aborting."; exit 1; }


echo "generating and Uploading SSH Key"
#generate local ssh file, upload to hetzner
retCode=$(ssh-keygen -t ed25519 -f ./sshkey -q -N "" <<< ""$'\n'"y")
#echo $?

ssh_value=$(<sshkey.pub) 

#upload key to hetzner
curlSSH=$(curl -s -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $API_TOKEN" \
-d "{\"name\": \"HTWK_Noten_Script_Key\", \"public_key\": \"$ssh_value\"}" \
'https://api.hetzner.cloud/v1/ssh_keys' | jq .error)


if [ "$curlSSH" != "null" ]
then
    echo "ERROR, KEY could not be installed!"
fi

echo "Generating VM"
#get Hostname/IP
curlVMIPv4=$(curl -s -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $API_TOKEN" \
-d '{"name": "HTWK-Noten-Server", "server_type": "cx11", "start_after_create": true, "image": "ubuntu-20.04", "ssh_keys": ["HTWK_Noten_Script_Key"], "user_data": "#cloud-config\nruncmd:\n- [touch, /root/cloud-init-worked]\n", "automount": false}' \
'https://api.hetzner.cloud/v1/servers' | jq -r '.server.public_net.ipv4.ip')

if [ "$curlVMIPv4" = "null" ]
then
    curlVMIPv4=$(curl -s -H "Authorization: Bearer $API_TOKEN" \
    'https://api.hetzner.cloud/v1/servers?name=HTWK-Noten-Server'| jq -r '.servers[0].public_net.ipv4.ip')

fi

sleep 25s

echo "installing git & pip"
ssh -q -o "StrictHostKeyChecking no" -i ./sshkey root@$curlVMIPv4 'export DEBIAN_FRONTEND=noninteractive; apt-get update && apt-get install -y git python3-pip' >/dev/null

echo "downloading Repo && install requirements"
ssh -q -o "StrictHostKeyChecking no" -i ./sshkey root@$curlVMIPv4 'mkdir -p repo; cd repo; git clone https://gitlab.com/tobix99/htwk-python-noten.git; cd htwk-python-noten; /usr/bin/python3 -m pip install -r ./requirements.txt' >/dev/null 

echo "uploading local varfile.py"
scp -q -o "StrictHostKeyChecking no" -i sshkey ./varfile.py root@$curlVMIPv4:~/repo/htwk-python-noten

echo "installing CRON Job"
ssh -q -o "StrictHostKeyChecking no" -i ./sshkey root@$curlVMIPv4 'touch /var/spool/cron/root && echo "*/10 * * * * /usr/bin/python3 ~/repo/htwk-python-noten/main.py > ~/log_noten.txt" >> /var/spool/cron/root && /usr/bin/crontab /var/spool/cron/root' >/dev/null 


echo "Testing Notifications"
ssh -q -o "StrictHostKeyChecking no" -i ./sshkey root@$curlVMIPv4 '/usr/bin/python3 ~/repo/htwk-python-noten/notification_test.py' >/dev/null