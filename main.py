import requests
from bs4 import BeautifulSoup
import telegram
import varfile

from tinydb import TinyDB, Query

# setup telegram Bot
bot = telegram.Bot(varfile.telegramBotToken)

# setup DB
db = TinyDB(varfile.fileName)

def getModuleName(cellArr):
    if cellArr[0][0] == "(":
        return cellArr[1]
    else:
        return cellArr[0]

with requests.session() as s:
    
    # set session cookie
    payload = {
        'username': varfile.username,
        'password': varfile.pw,
        'stg_role': 'S84',
        'submit': 'Anmelden'
    }
    s.post("https://qisserver.htwk-leipzig.de/qisserver/rds?state=user&type=1&category=auth.login&startpage=portal.vm&topitem=functions&breadCrumbSource=portal", data=payload)


    # get asi
    url_base = "https://qisserver.htwk-leipzig.de/qisserver/rds?state=user&type=0&menuid=uebersicht&navigationPosition=functions%2Cuebersicht&breadcrumb=uebersicht&topitem=functions&subitem=uebersicht"
    req = s.get(url_base)
    soup = BeautifulSoup(req.content, 'html.parser')

    rows = soup.find_all('a', class_='option')

    if len(rows) == 0 :
        bot.send_message(chat_id=varfile.telegramChatID, text="Something went wrong! Info: 1")
        exit()
    
    asi = rows[0].prettify().split("\n")[0].split(";asi=")[1][:20]

    url_without_asi = "https://qisserver.htwk-leipzig.de/qisserver/rds?state=notenspiegelStudent&next=list.vm&nextdir=qispos/notenspiegel/student&menuid=notenspiegelStudent&createInfos=Y&struct=auswahlBaum&nodeID=auswahlBaum%7Cabschluss%3Aabschl%3D84%2Cstgnr%3D1%7Cstudiengang%3Astg%3DTIB%7Csemester%3Aabschl%3D84%2Cstg%3DTIB%2Csemester%3D[YEAR][SEMESTER]&expand=0&asi=[ASI]#auswahlBaum%7Cabschluss%3Aabschl%3D84%2Cstgnr%3D1%7Cstudiengang%3Astg%3DTIB%7Csemester%3Aabschl%3D84%2Cstg%3DTIB%2Csemester%3D[YEAR][SEMESTER]"

    url_with_asi = url_without_asi.replace("[ASI]", asi).replace("[YEAR]", varfile.year).replace("[SEMESTER]", varfile.semester)


    # get grades
    req = s.get(url_with_asi)

    soup = BeautifulSoup(req.content, 'html.parser')

    rows = soup.find_all('tr', class_='MP')
    if len(rows) == 0: 
        bot.send_message(chat_id=varfile.telegramChatID, text="Something went wrong! Info: 2")
        exit()
    for row in rows:
        cell = row.find_all('td')

        ModulName = getModuleName(cell[1].get_text().strip().split("\n",2))
        Grade = cell[2].get_text().strip()

        # check if grade is already in QiS
        if Grade != "-" :
            Modul = Query()
            qu = db.search(Modul.name == ModulName)

            if len(qu) == 0:  
                db.insert({'name': ModulName, 'grade': Grade})
                #print(ModulName)
                #print(Grade)
                # Notifiy User
                msg = ModulName + " - " + Grade
                bot.send_message(chat_id=varfile.telegramChatID, text=msg)


