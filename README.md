# HTWK-Python-Noten
## Vorbedingungen
Es wird [Python3](https://www.python.org/downloads/), pip (mit `python3 -m pip -V` kann überprüft werden ob es installiert ist) und ein Telegram Bot ([BotFather](https://t.me/bodfather)) benötigt. Das ganze kann auch voll automatisch auf Hetzner in einer VM deployed werden und läuft dann 24/7. Dafür muss man sich einen Hetzner Cloud Account [erstellen](https://accounts.hetzner.com/signUp?_locale=de), dort ein neues Projekt anlegen und den Projektspezifischen API Key unter Pojektname/Sicherheit/API-Token kopieren. Die VM ist die kleinst mögliche (CX11) und kostet circa 2,90€ im Monat.

## Setup
Um das Script in Betrieb zu nehmen müssen in der Datei `varfile.py` in Zeile 6/7 die HTWK-Login Daten eingefügt und in Zeile 10 der API Token vom BotFather, sowie seine [ChatID](https://t.me/get_id_bot) in Zeile 11 und einen Dateinamen für eine "Textdatenbank" in Zeile 12 anggegeben werden. Diese wird dafür genutzt, um zu speichern, welches Modul bei der Letzten Anfrage schon eine Note veröffentlicht hatte.
Anschließend kann das Script einfach per python mit dem Befehl `python3 main.py` ausgeführt werden.
Dies kann mit einem CRON Job natürlich Automatisiert werden.

## Hetzner
Mit dem script `spin_up_hetzner.sh` kann auch einfach eine VM bei Hetzner eingerichtet werden, die 24/7 jede halbe Stunde checked, ob eine neue Note vorhanden ist und bei gelegenheit einen informiert. Dafür muss der Hetzner API Token in das File api.token eingefügt werden (dort ist ein Beispieltoken zu sehen, dieser kann einfach ersetzt werden) und danach kann das Script in Linux mit `./spin_up_hetzner.sh` ausgeführt werden. Für das Skript werden folgende Pakete benötigt: `curl, ssh, jq`. Diese können einfach per `apt-get install curl ssh jq` installiert werden. 